from decimal import Decimal

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from backend.consts import State

from .models import Discount, Tax


async def get_discount_percent(db: AsyncSession, amount: Decimal) -> Decimal:
    query = (
        select(
            Discount.discount_percent,
        )
        .where(
            Discount.min_cost <= amount,
        )
        .order_by(
            Discount.min_cost.desc(),
        )
        .limit(1)
    )

    row = (await db.execute(query)).first()
    return row.discount_percent if row else Decimal(0)


async def get_tax_percent(db: AsyncSession, state: State) -> Decimal:
    query = select(Tax.tax_percent).where(
        Tax.state == state,
    )

    result = await db.execute(query)
    return result.fetchone().tax_percent
