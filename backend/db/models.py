from sqlalchemy import Column, Enum, Integer, Numeric
from sqlalchemy.orm import DeclarativeMeta, declarative_base

from backend.consts import State

from .connection import engine

Base: DeclarativeMeta = declarative_base(bind=engine)


class Discount(Base):
    __tablename__ = "discount"

    id = Column(Integer, primary_key=True)  # noqa
    min_cost = Column(Numeric, nullable=False)
    discount_percent = Column(Numeric, nullable=False)
    __mapper_args__ = {"eager_defaults": True}


class Tax(Base):
    __tablename__ = "tax"

    id = Column(Integer, primary_key=True)  # noqa
    state = Column(
        Enum(
            State,
            values_callable=lambda enum: [enum_row.value for enum_row in enum],
            name="tax__state",
        ),
        nullable=False,
    )
    tax_percent = Column(Numeric, nullable=False)
    __mapper_args__ = {"eager_defaults": True}
