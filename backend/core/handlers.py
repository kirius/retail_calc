import typing as t
from dataclasses import dataclass

from sqlalchemy.ext.asyncio import AsyncSession

from backend.core.utils import get_percent_amount
from backend.db.managers import get_discount_percent, get_tax_percent
from backend.web.models import CostCalcRequest


@dataclass
class CalculatePriceHandler:
    db: AsyncSession

    async def process(self, request: CostCalcRequest) -> t.Dict[str, t.Any]:
        amount = request.price * request.items_count
        discount_percent = await get_discount_percent(self.db, amount)
        discount = get_percent_amount(amount, discount_percent)
        amount_with_discount = amount - discount
        tax_percent = await get_tax_percent(self.db, request.state)
        tax = get_percent_amount(amount_with_discount, tax_percent)

        return {
            "amount": amount,
            "discount_percent": discount_percent,
            "discount": discount,
            "amount_with_discount": amount_with_discount,
            "tax_percent": tax_percent,
            "tax": tax,
            "amount_total": amount_with_discount + tax,
        }
