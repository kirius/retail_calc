from decimal import Decimal


def get_percent_amount(amount: Decimal, percent: Decimal) -> Decimal:
    return (amount * percent / 100).quantize(Decimal("1.00"))
