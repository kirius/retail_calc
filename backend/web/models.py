from decimal import Decimal

from pydantic import BaseModel, ConstrainedDecimal, PositiveInt

from backend.consts import State


class PositiveDecimal(ConstrainedDecimal):
    gt = 0


class CostCalcRequest(BaseModel):
    class Config:
        schema_extra = {
            "example": {
                "items_count": 10,
                "price": 7.99,
                "state": "CA",
            },
        }

    items_count: PositiveInt
    price: PositiveDecimal
    state: State


class CostCalcResponse(BaseModel):
    amount: Decimal
    discount_percent: Decimal
    discount: Decimal
    amount_with_discount: Decimal
    tax_percent: Decimal
    tax: Decimal
    amount_total: Decimal
