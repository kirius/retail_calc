import typing as t

from fastapi import APIRouter, Depends, Request
from fastapi.responses import HTMLResponse, Response
from fastapi.templating import Jinja2Templates
from sqlalchemy.ext.asyncio import AsyncSession

from backend.core.handlers import CalculatePriceHandler
from backend.db.connection import async_session_maker

from .models import CostCalcRequest, CostCalcResponse

router = APIRouter()

templates = Jinja2Templates(directory="frontend/templates")


async def get_db() -> t.AsyncIterable[AsyncSession]:
    async with async_session_maker() as session:
        yield session


@router.post("/calculate-price", response_model=CostCalcResponse)
async def calculate_price(params: CostCalcRequest, db: AsyncSession = Depends(get_db)) -> t.Dict[str, t.Any]:
    return await CalculatePriceHandler(db).process(params)


@router.get("/", response_class=HTMLResponse)
async def index(request: Request) -> Response:
    return templates.TemplateResponse("index.html", {"request": request})
