from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from starlette.responses import PlainTextResponse

from config import settings

from . import views

app = FastAPI(
    debug=settings.debug,
)

app.include_router(views.router)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError) -> PlainTextResponse:
    return PlainTextResponse(str(exc), status_code=400)
