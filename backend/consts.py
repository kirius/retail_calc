from enum import Enum


class State(str, Enum):
    UT = "UT"
    NV = "NV"
    TX = "TX"
    AL = "AL"
    CA = "CA"
