black_options = --line-length 120 --target-version py39 --extend-exclude \alembic

web:
	python run_web.py

migrations:
	alembic upgrade head

requirements:
	pip install -r requirements-dev.txt

test:
	pytest --cov=backend --cov-fail-under 100 --blockage --cov-report term-missing

coverage-collect:
	coverage run -m pytest

coverage-report:
	coverage html

coverage: coverage-collect coverage-report

mypy:
	mypy backend tests *.py

flake8:
	flake8 .

isort:
	isort .

isort-check:
	isort . --check-only

black:
	black . $(black_options)

black-check:
	black . $(black_options) --check

fmt: isort black

check-all: isort-check flake8 black-check mypy test
