Retail calculaor
==========================
## Run with docker
1. install `docker` and `docker-compose`
2. Run `docker-compose up`
3. visit `http://127.0.0.1:8005/`
## Setup on host

1. create python virtual environment with the tool of your choice(tested only on python 3.9)
2. install dependencies: `make requirements`
3. create `.env` file with correct `DATABASE_URL` (see `.env.sample` for example) 
4. apply migrations: `make migrations`
5. Run web server: `make web`
6. visit `http://127.0.0.1:8005/`

## Useful links and commands
API documentation: http://127.0.0.1:8005/docs

Run tests: `make test`

Run all checks: `make check-all`

You can also run any command from `Makefile` in docker, for example to run tests in docker:
`docker-compose run web make test` (make sure db is up and running or tests will fail)
