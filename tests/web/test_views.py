from decimal import Decimal
from unittest.mock import patch

import pytest
from httpx import AsyncClient

from backend.consts import State


@pytest.mark.asyncio
async def test_calculate_price_no_discount_and_tax(client: AsyncClient) -> None:
    req_data = {
        "items_count": 10,
        "price": 20,
        "state": State.CA,
    }

    resp_data = {
        "amount": Decimal(20),
        "discount_percent": Decimal(0),
        "discount": Decimal(0),
        "amount_with_discount": Decimal(200),
        "tax_percent": Decimal(0),
        "tax": Decimal(0),
        "amount_total": Decimal(200),
    }

    with patch("backend.web.views.CalculatePriceHandler.process", return_value=resp_data):
        response = await client.post("calculate-price", json=req_data)

        assert response.status_code == 200
        assert response.json() == resp_data


@pytest.mark.asyncio
async def test_calculate_price_invalid_request(client: AsyncClient) -> None:
    req_data = {
        "items_count": -10,
        "price": 1,
        "state": "AA",
    }

    response = await client.post("calculate-price", json=req_data)
    assert response.status_code == 400
    error_msg = response.text

    assert "items_count" in error_msg
    assert "state" in error_msg
    assert "price" not in error_msg


@pytest.mark.asyncio
async def test_index(client: AsyncClient) -> None:
    response = await client.get("/")
    assert response.status_code == 200
