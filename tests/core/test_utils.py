from decimal import Decimal

import pytest

from backend.core.utils import get_percent_amount


@pytest.mark.parametrize(
    ("amount", "discount_percent", "result"),
    [
        (Decimal(0), Decimal(100), Decimal(0)),
        (Decimal(100), Decimal(10), Decimal(10)),
        (Decimal(100), Decimal("10.99"), Decimal("10.99")),
        (Decimal("100.5"), Decimal(100.0), Decimal("100.5")),
    ],
)
def test_get_percent_amount(amount: Decimal, discount_percent: Decimal, result: Decimal) -> None:
    assert get_percent_amount(amount, discount_percent) == result
