from decimal import Decimal
from unittest.mock import MagicMock, patch

import pytest

from backend.consts import State
from backend.core.handlers import CalculatePriceHandler
from backend.web.models import CostCalcRequest


@pytest.mark.asyncio
async def test_calculate_price_no_discount_and_tax() -> None:
    request = CostCalcRequest(
        items_count=10,
        price=100,
        state=State.CA,
    )

    with patch("backend.core.handlers.get_tax_percent", return_value=0), patch(
        "backend.core.handlers.get_discount_percent", return_value=0
    ):
        response = await CalculatePriceHandler(MagicMock()).process(request)

        assert response == {
            "amount": Decimal(1000),
            "discount_percent": Decimal(0),
            "discount": Decimal(0),
            "amount_with_discount": Decimal(1000),
            "tax_percent": Decimal(0),
            "tax": Decimal(0),
            "amount_total": Decimal(1000),
        }


@pytest.mark.asyncio
async def test_calculate_price_with_discount_and_tax() -> None:
    request = CostCalcRequest(
        items_count=10,
        price=Decimal("100.5"),
        state=State.CA,
    )

    with patch("backend.core.handlers.get_tax_percent", return_value=Decimal("10.5")), patch(
        "backend.core.handlers.get_discount_percent", return_value=Decimal("5.5")
    ):
        response = await CalculatePriceHandler(MagicMock()).process(request)

        assert response == {
            "amount": Decimal(1005),
            "discount_percent": Decimal("5.5"),
            "discount": Decimal("55.28"),
            "amount_with_discount": Decimal("949.72"),
            "tax_percent": Decimal("10.5"),
            "tax": Decimal("99.72"),
            "amount_total": Decimal("1049.44"),
        }
