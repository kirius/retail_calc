import asyncio
import typing as t

import pytest
from httpx import AsyncClient
from sqlalchemy import text
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine

from backend.db.connection import engine
from backend.db.models import Base
from backend.web.app import app
from config import settings

test_db_url = f"{settings.database_url}_test"


@pytest.fixture()
async def client() -> t.AsyncIterable[AsyncClient]:
    async with AsyncClient(app=app, base_url="http://test") as client:
        yield client


@pytest.fixture(scope="session")
def create_test_db() -> None:
    asyncio.run(_create_test_db())


async def _create_test_db() -> None:
    async with engine.connect() as conn:
        await conn.execute(text("rollback"))
        await conn.execute(text(f"DROP DATABASE IF EXISTS {engine.url.database}_test"))
        await conn.execute(text(f"CREATE DATABASE {engine.url.database}_test"))

    test_engine = create_async_engine(test_db_url, echo=True)

    async with test_engine.begin() as test_conn:
        await test_conn.run_sync(Base.metadata.create_all)


@pytest.fixture()
async def db(create_test_db: t.Literal[None]) -> t.AsyncIterable[AsyncSession]:
    test_engine = create_async_engine(test_db_url, echo=True)

    async with AsyncSession(test_engine, expire_on_commit=False) as session:
        yield session
