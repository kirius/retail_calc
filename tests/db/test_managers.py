from decimal import Decimal

import pytest
from sqlalchemy import insert
from sqlalchemy.ext.asyncio import AsyncSession

from backend.consts import State
from backend.db.managers import get_discount_percent, get_tax_percent
from backend.db.models import Discount, Tax


@pytest.mark.parametrize(
    ("amount", "expected_discount_percent"),
    [
        (Decimal(100), Decimal(0)),
        (Decimal(999), Decimal(0)),
        (Decimal(1000), Decimal(5)),
        (Decimal(1001), Decimal(5)),
        (Decimal(1999), Decimal(5)),
        (Decimal(2000), Decimal(10)),
        (Decimal(20000), Decimal(10)),
    ],
)
@pytest.mark.asyncio
async def test_get_discount_percent(db: AsyncSession, amount: Decimal, expected_discount_percent: Decimal) -> None:
    await db.execute(insert(Discount).values(min_cost=1000, discount_percent=5))
    await db.execute(insert(Discount).values(min_cost=2000, discount_percent=10))

    assert await get_discount_percent(db, amount) == expected_discount_percent


@pytest.mark.parametrize(
    ("state", "expected_tax_percent"),
    [
        (State.CA, Decimal("10.7")),
        (State.AL, Decimal("15")),
    ],
)
@pytest.mark.asyncio
async def test_get_tax_percent(db: AsyncSession, state: State, expected_tax_percent: Decimal) -> None:
    await db.execute(insert(Tax).values(state=State.CA, tax_percent=Decimal("10.7")))
    await db.execute(insert(Tax).values(state=State.AL, tax_percent=15))

    assert await get_tax_percent(db, state) == expected_tax_percent
