FROM python:3.9.7-slim-buster

ARG REQUIREMENTS=requirements.txt

RUN apt-get update && \
    apt-get install -y wait-for-it make && \
    rm -rf /var/lib/apt/lists/*

# set the working directory in the container
WORKDIR /app

# copy the dependencies file to the working directory
COPY requirements*.txt ./

# install dependencies
RUN pip install -r ${REQUIREMENTS}

# copy the content of the local directory to the working directory
COPY . /app
