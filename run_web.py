import uvicorn

from config import settings

if __name__ == "__main__":
    uvicorn.run(
        "backend.web.app:app",
        reload=settings.debug,
        host=settings.service_host,
        port=settings.service_port,
    )
