import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    class Config:
        env_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), ".env")

    debug: bool = False
    base_dir: str = os.path.dirname(os.path.abspath(__file__))
    service_host: str = "127.0.0.1"
    service_port: int = 8005
    database_url: str


settings = Settings()
